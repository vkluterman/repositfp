import React, {Component} from 'react';
import GuestListChekItem from '../guest-list-chek-item/guest-list-chek-item';
import EnterMessage from '../EnterMessage/enter-message';
import "./guest-list-chek.css";

export default class GuestListChek extends Component {




    


render () {

const { guestLists} = this.props;


const elements = guestLists.map((item) => {   
    
    return (
        <li key={item.id}
        className={item.listStyle}>
         <GuestListChekItem 
             { ... item }
             />
             <EnterMessage 
             {... item}
             />
        </li>
            )
});

    return  (

      
        

        <ul className="list-chek-ul">           
            {elements}  
        </ul>
        )
    };
           
}

