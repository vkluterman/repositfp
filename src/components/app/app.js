import React, { Component } from 'react';
import AppHeader from '../app-header/app-header';
import GuestListForm from '../guest-list-form/guest-list-form';
import GuestListChek from '../guest-list-chek/guest-list-chek';
import {db} from '../app/firebase';


export default class App extends Component {

    maxId = 100;

        state = {
        localStorageBaseRender: 
        [        

            {label: 'Ты хочешь заняться фитнесом?', id: 1, listStyle: 'answer' },
       
        ]
    }

     answerBase = [        
            {label: 'Ты хочешь заняться фитнесом?', id: 1, listStyle: 'answer' },
            {label: 'Как ты это видишь:', id: 2, listStyle: 'answer' },
            {label: 'Какая польза тебе в этом:', id: 3, listStyle: 'answer' },
            {label: 'Какой новый способ достичь эту пользу и что может смотивировать тебя на достижение этой пользы:', id: 4, listStyle: 'answer' },
            {label: '...', id: 5, listStyle: 'answer' },
       
        ]


    addItem = (text) => {

        const newItem = {
            label: text,
             id: this.maxId++,
             listStyle: "message"
        };

if (newItem.id < 104) {
    this.setState(({localStorageBaseRender}) => {
        console.log(newItem.id);

        const idAddItem = newItem.id - 99;
        const oldItem = this.answerBase[idAddItem];

        const newArr = [
            ...localStorageBaseRender,
            newItem,
            oldItem
          
        ];

        return {
            localStorageBaseRender: newArr   
        };     
    });

        } else {
            console.log('the end')
        }

    };

// **************************************************
    // Sending to the server

    componentDidUpdate() {

    const itemForDB = this.state.localStorageBaseRender
    const dataId = new Date().getTime();

        db.collection("chatAnswer").doc(dataId.toString()).set({
            name: itemForDB
           
        })
        .then(function() {
            // console.log("Document successfully written!");
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
        });

    }
// **************************************************




    render() {
       

        return (
            <div className="app">
            <AppHeader label="Fitness Psyholog" />
            <GuestListForm 
                onItemAdded={this.addItem}
           
                />      
            <GuestListChek 
                guestLists={this.state.localStorageBaseRender} 

                /> 
            </div>
        );
        }
    };



 

 