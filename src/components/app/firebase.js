import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  // copy and paste your firebase credential here
  apiKey: "AIzaSyBGzJew_38Bh_RZ7Yso6Q6cl4qHMH1KWL8",
  authDomain: "fitgoweb.firebaseapp.com",
  databaseURL: "https://fitgoweb.firebaseio.com",
  projectId: "fitgoweb",
  storageBucket: "fitgoweb.appspot.com",
  messagingSenderId: "765156513917",
  appId: "1:765156513917:web:b29ca02e0a01a77c1ab914",
  measurementId: "G-446CWHN8JJ"
});

const db = firebaseApp.firestore();

export { db };